﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour{

    public const int LAYER_INTERACTABLE = 8;
    public const int LAYERMASK_INTERACTABLE = 1 << 8;

    public bool pauseTimer = false;
    public FirstPersonController firstPersonController;
    public PlayerScript playerScript;
    public Image crosshairImage;
    public Sprite normalCrosshair;
    public Sprite pistolCrosshair;



    #region singleton
    private static GameManager instance;
    private GameManager() { }

    public static GameManager GetInstance() {
        return instance;
    }
    #endregion




    #region lifecycle
    private void Awake() {
        if (instance == null) {
            instance = this;
            GameObject playerGameObject = GameObject.Find("Player");
            firstPersonController = playerGameObject.GetComponent<FirstPersonController>();
            playerScript = playerGameObject.GetComponent<PlayerScript>();
        } else {
            if (instance != this) {
                Destroy(gameObject);
            }
        }
    }
    #endregion



    #region helper
    public void ShowCursor( bool show ) {
        if (show) {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            firstPersonController.m_MouseLook.lockCursor = false;
        } else {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            firstPersonController.m_MouseLook.lockCursor = true;
        }
    }

    public void SetActiveMouseLookAndHover( bool active ) {
        if (active) {
            playerScript.mouseHoverEnabled = true;
            firstPersonController.m_MouseLook.XSensitivity = playerScript.playerData.mouseSensitivityX;
            firstPersonController.m_MouseLook.YSensitivity = playerScript.playerData.mouseSensitivityY;
        } else {
            playerScript.mouseHoverEnabled = false;
            firstPersonController.m_MouseLook.XSensitivity = 0;
            firstPersonController.m_MouseLook.YSensitivity = 0;
        }
    }

    public void SetCrosshairImageToPistol(bool toPistol) {
        if (toPistol)
            crosshairImage.sprite = pistolCrosshair;
        else {
            if (crosshairImage.sprite != normalCrosshair) { // only change if necessary
                crosshairImage.sprite = normalCrosshair;
            }
        }
    }
    #endregion
}

