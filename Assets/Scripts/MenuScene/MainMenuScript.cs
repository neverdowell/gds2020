﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUIScript : MonoBehaviour{

    #region lifecycle
    void Start(){
        
    }

    void Update(){
        
    }


    #endregion



    #region ui callbacks
    public void ChangeToScene(string sceneName ) {
        SceneManager.LoadScene(sceneName);
    }
    #endregion




    #region helper


    #endregion
}
