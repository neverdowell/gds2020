﻿using UnityEngine;

/// <summary>
/// "Open lock" QuestAction implementation
/// </summary>
public class OpenLockQuestAction : AQuestAction {

    public TurnLockWheelQuestAction[] turnLockWheelQuestActions;
    public int[] lockCode;




    public override void PerformQuestAction(int questActionId ) {
        // check if code is right
        for (int i = 0; i < lockCode.Length; i++) {
            TurnLockWheelQuestAction turnLockWheelQuestAction = turnLockWheelQuestActions[i];
            if (turnLockWheelQuestAction.currentWheelStep != lockCode[i]) {
                // Wrong code
                MessageManager.GetInstance().showMessage("Der Zahlencode ist falsch.");
                return;
            }
        }
        // destroy lock
        MessageManager.GetInstance().showMessage("Das Schloss ist offen.");
        Destroy(transform.parent.gameObject);
        // open left/right door
        iTween.RotateBy(questActionGameObjects[0], new Vector3(0, 0.3f, 0), 1f); // linke Tür
        //iTween.RotateBy(questActionGameObjects[1], new Vector3(0, -0.3f, 0), 1f); // rechte Tür
    }
}
