﻿using UnityEngine;

/// <summary>
/// "08 Wasser abpumpen" QuestAction implementation
/// </summary>
public class FahrtstuhlFahrtQuestAction : AQuestAction {

    public float verticalShakeDuration = 0.2f;
    public float verticalShakeStrength = 0.3f;
    public Vector3 movementTargetPosition;
    public float movementSpeed = 1f;

    public override void PerformQuestAction(int questActionId ) {
        // shake cabin before movement
        iTween.ShakePosition(questActionGameObjects[0], Vector3.up * verticalShakeStrength, verticalShakeDuration);

        // move cabin after shaking
        iTween.MoveTo(questActionGameObjects[0], iTween.Hash(
            "delay", verticalShakeDuration,
            "position", movementTargetPosition,
            "speed", movementSpeed,
            "easeType", "linear"
            ));
        // play sound -> shake
        // play sound -> Lift movement
    }
}
