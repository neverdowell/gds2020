﻿using UnityEngine;

/// <summary>
/// "09 Sand sieben" QuestAction implementation
/// </summary>
public class SandSiebenQuestAction : AQuestAction {
    private GameObject siebGameObject;
    private Vector3 resetPosition;
    private Vector3 resetRotation;

    private void OnEnable() {
        siebGameObject = questActionGameObjects[0];
        resetPosition = siebGameObject.transform.position;
        resetRotation = siebGameObject.transform.rotation.eulerAngles;
    }



    public override void PerformQuestAction(int questActionId ) {
        // Sieb sichtbar machen
        siebGameObject.SetActive(true); 
        // Erstelle Animationen
        CreateAnimation();
    }



    public void CreateAnimation() {
        // Erstes Sieben
        iTween.MoveBy(siebGameObject, iTween.Hash(
                                        "x", -2,
                                        "time", 2,
                                        "easeType", "linear"
                                        ));
        iTween.RotateBy(siebGameObject, iTween.Hash(
                                        "z", -0.18f, 
                                        "time", 2,
                                        "easeType", "easeInOutQuad"
                                        ));
        //iTween.MoveTo(siebGameObject, iTween.Hash(
        //                                "delay", 2.1f,
        //                                "position", resetPosition,
        //                                "time", 0.5f,
        //                                "easeType", "easeInOutQuad",
        //                                "islocal", true
        //                                ));
        //iTween.RotateTo(siebGameObject, iTween.Hash(
        //                                "delay", 2.1f,
        //                                "rotation", resetRotation,
        //                                "time", 0.5f,
        //                                "easeType", "easeInOutQuad",
        //                                "islocal", true
        //                                ));

        Invoke("resetSieb", 2.25f);
        Invoke("schlüsselSichtbarMachen", 3.1f);
        // Zweites Sieben
        iTween.MoveBy(siebGameObject, iTween.Hash(
                                        "delay", 2.8f,
                                        "x", -2,
                                        "time", 2,
                                        "easeType", "linear"
                                        //"oncompletetarget", gameObject,
                                        //"oncomplete", "refillWater"
                                        ));
        iTween.RotateBy(siebGameObject, iTween.Hash(
                                        "delay", 2.8f,
                                        "z", -0.18f,
                                        "time", 2,
                                        "easeType", "easeInOutQuad"
                                        ));
    }

    public void resetSieb() {
        Debug.Log("resetSieb");
        siebGameObject.transform.position = resetPosition;
        siebGameObject.transform.rotation = Quaternion.Euler(resetRotation);
    }

    public void schlüsselSichtbarMachen() {
        Debug.Log("resetSieb");
        questActionGameObjects[1].SetActive(true);
    }
}
