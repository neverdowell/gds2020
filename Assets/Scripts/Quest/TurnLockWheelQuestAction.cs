﻿using UnityEngine;

/// <summary>
/// "Turn lock wheel" QuestAction implementation
/// </summary>
public class TurnLockWheelQuestAction : AQuestAction {

    public int wheelStepCount = 4;
    public int currentWheelStep = 0;
    public int degreePerStep;

    private void OnEnable() {
        degreePerStep = 360 / wheelStepCount;
    }



    public override void PerformQuestAction(int questActionId ) {
        transform.Rotate(new Vector3(0, degreePerStep, 0));
        currentWheelStep = (currentWheelStep + 1) % wheelStepCount;
        Debug.Log("Curent Lockwheel value: " + GetCurrentLockWheelValue());
    }

    public int GetCurrentLockWheelValue() {
        return currentWheelStep;// (int) Mathf.Round( transform.rotation.y / wheelStepCount);
    }
}
