﻿using UnityEngine;

/// <summary>
/// "Batterie In Radio Einsetzen" QuestAction implementation
/// </summary>
public class BatterieInRadioEinsetzenQuestAction : AQuestAction {

    public override void PerformQuestAction(int questActionId ) {
        questActionGameObjects[0].GetComponent<PlayerScript>().turnOnRadio();
    }

}
