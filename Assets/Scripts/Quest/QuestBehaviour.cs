﻿using System;
using UnityEngine;

public class QuestBehaviour : MonoBehaviour{

    public QuestData questData;
    public AQuestAction questAction;
    public GameObject[] toggleVisibility;



    private InventoryManager inventoryManager;



    #region lifecycle
    private void Start() {
        gameObject.layer = GameManager.LAYER_INTERACTABLE;   // make gameobject interactable by default (for lazy level designers ;) )
        inventoryManager = InventoryManager.GetInstance();
    }
    #endregion


    #region helper
    public void TryPerformAction(Action action, ItemData selectedItemData ) {
        // test, if this is a required action (use, take or open)
        if (!questData.successfullActions.Contains(action)) {
            Debug.Log(questData.failMessage + " " + Enum.GetName(typeof(Action), action));
            return;
        }

        // test, if
        if (!CanPerformAction(selectedItemData)) {
            //Debug.Log("Quest '" + questData.name + "' failed.");
            MessageManager.GetInstance().showMessage(questData.failMessage);
            return;
        }

        //// test, if
        //if (!CanPerformAction()) {
        //    //Debug.Log("Quest '" + questData.name + "' failed.");
        //    MessageManager.GetInstance().showMessage(questData.failMessage);
        //    return;
        //}

        if (questData.name != null) {
            //Debug.Log("Quest '" + questData.name + "' solved.");
            MessageManager.GetInstance().showMessage(questData.successMessage);
        }

        // remove Items
        foreach (ItemData itemData in questData.RemoveFromInventory) {
            inventoryManager.RemoveFromInventory(itemData);
        }
        // addItems
        foreach (ItemData itemData in questData.AddToInventory) {
            inventoryManager.AddToInventory(itemData);
        }

        // toggle active
        foreach (GameObject tempGameObject in toggleVisibility) {
            tempGameObject.SetActive(!tempGameObject.activeSelf);
        }

        // destroy if necessary
        if (questData.destroyInteractable) {
            Destroy(this.gameObject);
        }

        // spawn Object
        if (questData.spawnGameObject != null) {
            Quaternion quaternion = Quaternion.Euler(questData.spawnRotation);
            Instantiate(questData.spawnGameObject, questData.spawnPosition, quaternion);
        }
        if (questAction != null) {
            questAction.PerformQuestAction(0);
        }
    }

    public bool CanPerformAction() {
        foreach (ItemData itemData in questData.requiredItems) {
            if (!inventoryManager.ContainsInInventory(itemData)) {
                //Debug.Log("Quest '" + questData.name + "' failed.");
                MessageManager.GetInstance().showMessage(questData.failMessage);
                return false;
            }
        }
        return true;
    }

    public bool CanPerformAction(ItemData selectedItemData) {
        // if no item is required return true
        if (questData.requiredItems.Length == 0) {
            return true;
        }

        // if item is required, but non is selected
        if (selectedItemData == null) {
            return false;
        }

        // if requiredItem is selected return true
        foreach (ItemData itemData in questData.requiredItems) {
            if(itemData.itemId == selectedItemData.itemId) {
                return true;
            } 
        }
        Debug.Log("QuestBehaviour.CanPerformAction: Quest '" + questData.name + "': SelectedItemData is missing.");
        return false;
    }
    #endregion
}

