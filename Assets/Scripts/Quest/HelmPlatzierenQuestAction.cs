﻿using UnityEngine;

/// <summary>
/// "08 Wasser abpumpen" QuestAction implementation
/// </summary>
public class HelmPlatzierenQuestAction : AQuestAction {

    public override void PerformQuestAction(int questActionId ) {
        questActionGameObjects[0].SetActive(true); // show helmet
        questActionGameObjects[1].SetActive(true); // show Bird

        iTween.ScaleTo(questActionGameObjects[1], new Vector3(0.3f, 0.3f, 0.3f), 1f);
        iTween.MoveTo(questActionGameObjects[1], new Vector3(-29.63f, -23.7f, -117f), 2f); // MoveBird
    }
}
