﻿using UnityEngine;

/// <summary>
/// This must be implemented for each QuestAction like Animation or complex behaviour.
/// Since a QuestAction may perform several different actions, you can use questActionId a indicator, which Action is ment.
/// </summary>
public class OpenTableDrawerQuestAction : AQuestAction {

    public override void PerformQuestAction( int questActionId ) {
        // open drawer animation
        iTween.MoveBy(questActionGameObjects[0], new Vector3(1, 0, 0), 0.5f);
    }
}
