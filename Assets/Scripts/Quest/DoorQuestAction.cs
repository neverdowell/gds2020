﻿using UnityEngine;

/// <summary>
/// This must be implemented for each QuestAction like Animation or complex behaviour.
/// Since a QuestAction may perform several different actions, you can use questActionId a indicator, which Action is ment.
/// </summary>
public class DoorQuestAction : AQuestAction {
    //public GameObject[] questActionGameObjects;
    public DoorMovementScript doorMovementScript;

    public override void PerformQuestAction( int questActionId ) {
        doorMovementScript.moveDoor();
    }
}
