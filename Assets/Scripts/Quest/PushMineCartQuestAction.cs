﻿using UnityEngine;

/// <summary>
/// This must be implemented for each QuestAction like Animation or complex behaviour.
/// Since a QuestAction may perform several different actions, you can use questActionId a indicator, which Action is ment.
/// </summary>
public class PushMineCartQuestAction : AQuestAction {
    //public GameObject[] questActionGameObjects;

    public MineCartMovementScript mineCartMovementScript;




    public override void PerformQuestAction( int questActionId ) {
        if (mineCartMovementScript.isWheelRepaired) {
            if (!mineCartMovementScript.hasBeenMoved) { 
                mineCartMovementScript.PushMineCart();
                MessageManager.GetInstance().showMessage("Die Lore beginnt zu rollen...");
            } else {
                MessageManager.GetInstance().showMessage("Die Lore steckt fest.");
            }
        } else {
            MessageManager.GetInstance().showMessage("Irgendetwas ist noch kaputt.");
        }
    }
}
