﻿using UnityEngine;

/// <summary>
/// This must be implemented for each QuestAction like Animation or complex behaviour.
/// Since a QuestAction may perform several different actions, you can use questActionId a indicator, which Action is ment.
/// </summary>
public class PickupDiaryQuestAction : AQuestAction {


    public override void PerformQuestAction( int questActionId ) {
        // show diary for the first time
        PlayerScript playerScript = questActionGameObjects[0].GetComponent<PlayerScript>();
        playerScript.isDiaryCollected = true;
        playerScript.showDiary(true);
        // show Text
        MessageManager.GetInstance().showMessage("Drücke '" + playerScript.playerData.tagebuchKeyCode + "' zum Zeigen des Tagebuchs.");
    }
}
