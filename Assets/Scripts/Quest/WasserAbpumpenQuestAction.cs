﻿using UnityEngine;

/// <summary>
/// "08 Wasser abpumpen" QuestAction implementation
/// </summary>
public class WasserAbpumpenQuestAction : AQuestAction {
    public override void PerformQuestAction(int questActionId ) {
        Debug.Log("PerformQuestAction " + questActionId);
        GameObject wasserGameObject = questActionGameObjects[0];
        iTween.MoveBy(wasserGameObject, iTween.Hash(
                                        "y",                -5,
                                        "time",             5,
                                        "easeType",         "linear",
                                        "oncompletetarget", gameObject,
                                        "oncomplete", "refillWater"
                                        ));
    }

    public void refillWater() {
        Debug.Log("PerformQuestAction: RefillWater");
        GameObject wasserGameObject = questActionGameObjects[0];
        iTween.MoveBy(wasserGameObject, iTween.Hash(
                                        "delay", 10,
                                        "y", 5,
                                        "time", 20,
                                        "easeType", "linear"
                                        ));
    }
}
