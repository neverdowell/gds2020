﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public List<ItemData> inventory = new List<ItemData>();
    public ItemData selectedItemData;
    public Image selectedItemImage;

    public InventoryUIScript inventoryUIScript;


    private GameManager gameManager;


    #region singleton
    private static InventoryManager instance;
    private InventoryManager() { }

    public static InventoryManager GetInstance() {
        return instance;
    }
    #endregion




    #region lifecycle
    private void Awake() {
        if (instance == null) {
            instance = this;
            //DontDestroyOnLoad(this.gameObject);
        } else {
            if (instance != this) {
                Destroy(this.gameObject);
            }
        }
    }

    private void Start() {
        gameManager = GameManager.GetInstance();
    }
    #endregion




    #region Inventory Feature
    public void AddToInventory( ItemData itemData ) {
        if (!ContainsInInventory(itemData)) {
            inventory.Add(itemData);
            //Debug.Log(itemData.name + " aufgehoben");
            MessageManager.GetInstance().showMessage(itemData.name + " aufgehoben");
        } else {
            //Debug.Log("Inventory yet does contain inventoryItem: " + itemData.name);
        }
    }

    public bool ContainsInInventory( ItemData itemData ) {
        return inventory.Contains(itemData);
    }

    public void RemoveFromInventory( ItemData itemData ) {
        if (ContainsInInventory(itemData)) {
            inventory.Remove(itemData);
        } else {
            //Debug.Log("Inventory does not contain inventoryItem: " + itemData);
        }
    }

    public void SelectItem( ItemData selectedItemData ) {
        this.selectedItemData = selectedItemData;
        if (selectedItemData != null) {
            selectedItemImage.enabled = true;
            selectedItemImage.sprite = selectedItemData.sprite;
            if (selectedItemData.name.Equals("Pistole")) { // if item is pistol -> change crosshair to pistol
                gameManager.SetCrosshairImageToPistol(true);
                selectedItemImage.enabled = false;
            }
        } else {
            selectedItemImage.enabled = false;
            gameManager.SetCrosshairImageToPistol(false);
        }
        ShowInventory(false);
    }

    public void ShowInventory( bool show ) {
        inventoryUIScript.RefreshInventory();
        inventoryUIScript.gameObject.SetActive(show);
        gameManager.ShowCursor(show);
        gameManager.SetActiveMouseLookAndHover(!show);
    }
    #endregion
}
