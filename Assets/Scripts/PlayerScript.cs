﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerScript : MonoBehaviour{

    [Header("Settings")]
    public PlayerData playerData;
    public bool resetToDefaultStartPosition = true;
    public Vector3 defaultStartPosition = new Vector3(0, 2, 0);
    public bool isMenuScene = false;
    public FadingLayerScript fadingLayerScript;
    public bool mouseHoverEnabled = true;
    public bool isDead = false;
    public CharacterController characterController;
    public FirstPersonController firstPersonController;


    [Header("Injury")]
    public bool isInjured = false;
    public FadingLayerScript redFadingLayerScript;
    public float injuriedFadingInterval = 1.0f;

    [Header("Inventory Feature")]
    public GameObject inventory;
    public GameObject inventoryItemPrefab;
    public ItemData selectedItemData;

    [Header("BirdCage Feature")]
    public GameObject birdCageIndicator;
    public RawImage birdCageIndicatorRawImage;
    public bool birdCageIndicatorActive = false;
    public ItemData birdCageItemData;

    [Header("Flashlight Feature")]
    public GameObject flashlight;
    public Light flashlightLight;
    public ItemData flashlightWithBatteryItemData;
    public float flashlightDuration = 10f;
    public float flashlightFadeOut = 3f;
    public bool isflashlightOn = false;

    [Header("Diary Feature")]
    public bool isDiaryCollected = false;
    public GameObject diary;
    public RawImage diaryRawImage;
    //public ItemData diaryItemData;

    [Header("Radio Feature")]
    public GameObject radio;
    public GameObject radioAudioSourceGameObject;
    public float radioPlayingDuration;
    public bool isRadioTurnedOn = false;

    [Header("Zahlenschloss")]
    public Text zalenCodeText;
    public OpenLockQuestAction openLockQuestAction;


    [Header("UI")]
    public Camera fpsCamera;
    public Text questItemNameText;

    


    private GameManager gameManager;
    private InventoryManager inventoryManager;
    private RaycastHit raycastHit;
    private float currentRadioPlayingDuration;
    private MessageManager messageManager;




    #region lifecycle
    private void Awake() {
        if (resetToDefaultStartPosition) {
            characterController.enabled = false;
            characterController.transform.position = defaultStartPosition;
            characterController.enabled = true;
        }
    }


    private void Start(){
        inventoryManager = InventoryManager.GetInstance();
        if (SceneManager.GetActiveScene().name.Equals("MenuScene")) {
            isMenuScene = true;
        } else {
            isMenuScene = false;
            InitZahlenSchloss();
        }
        fadingLayerScript.fadeLayerOut();
        gameManager = GameManager.GetInstance();
        messageManager = MessageManager.GetInstance();
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.P)) {
            transform.position = defaultStartPosition;
        }

        if (mouseHoverEnabled) { // XXX HIER IST DER FEHLER
            tryHitObject();
            if (isMenuScene){
               return;
            }
                if (Input.GetKeyDown(playerData.primaryAction)) {
                if (raycastHit.collider != null) { // Prüfen, ob es der richtige Layer ist
                    QuestBehaviour questBehaviour = raycastHit.collider.gameObject.GetComponent<QuestBehaviour>();
                    if (questBehaviour != null) {
                        if (questBehaviour.questData != null) {
                            if (questBehaviour.CanPerformAction()) {
                                questBehaviour.TryPerformAction(Action.Take, inventoryManager.selectedItemData);
                            }
                        }
                    }
                }
                // deselect item on click
                if (!inventory.activeSelf) { 
                    InventoryManager.GetInstance().SelectItem(null);
                }
            }
        }

        if (Input.GetKeyDown(playerData.tagebuchKeyCode)) {
            if (isDiaryCollected) { 
            Debug.Log("PlayerScript.Update: Todo: Check for Tagebuch, before using");
            toggleShowDiary();
            } else {
                MessageManager.GetInstance().showMessage("Nichts zu lesen dabei");
            }
        } 

        if (Input.GetKeyDown(playerData.inventoryKeyCode)) {
            if (inventory.activeSelf) {
                inventoryManager.ShowInventory(false);
                gameManager.SetActiveMouseLookAndHover(true);
            } else {
                inventoryManager.ShowInventory(true);
                gameManager.SetActiveMouseLookAndHover(false);
            }
        }

        // cancle/deselect
        if (Input.GetKeyDown(playerData.cancelKeyCode)) {
            // if item selected deselect
            // and if inventory is open close it
            if (inventoryManager) { 
                inventoryManager.SelectItem(null);      // deselect Item
                inventoryManager.ShowInventory(false);  // hide inventory
            }
            // if diary open close
            showDiary(false);                       // hide diary
            //SetActiveMouseLookAndHover(true);
        }

        // testing only
        updateFlashlight();
    }

    private void OnTriggerEnter(Collider collider) {
        switch (collider.name) {
            case "ChangeToGameTrigger":
                fadingLayerScript.fadeLayerIn();
                Invoke("loadGameScene", fadingLayerScript.fadingDelay + fadingLayerScript.fadingDuration);
                break;
            case "InjuringCollider":
                if (!isInjured) { 
                    setInjured(true);
                    redFadingLayerScript.InvokeRepeating("fadeLayerOut", injuriedFadingInterval, injuriedFadingInterval);
                    messageManager.showMessage("Autsch, mein Knöchel.");
                }
                break; 
            case "DieCollider":
                Die();
                break;
            case "WinningCollider":
                fadingLayerScript.fadeLayerIn();
                Invoke("loadGameEndScene", fadingLayerScript.fadingDuration);
                break;
            default:
                Debug.Log("Ignore TriggerEnter on " + collider.name);
                break;
        }
    }
    #endregion



    #region features
    public void activateFlashlight() {
        // check if flashlight is in Inventory
        if (!inventoryManager.ContainsInInventory(flashlightWithBatteryItemData)) {
            messageManager.showMessage("Ich habe nichts, um Licht zu machen.");
            return;
        } 

        if (flashlightDuration > 0) {
            if (!isflashlightOn) { 
                isflashlightOn = true;
                flashlight.SetActive(true);
                messageManager.showMessage("Die Taschenlampe ist an.");
            }
        } else {
            messageManager.showMessage("Die Batterie ist alle.");
        }
    }

    public void updateFlashlight() {
        if (isflashlightOn) {
            flashlightDuration -= Time.deltaTime;
            if (flashlightDuration < 0) {
                isflashlightOn = false;
                flashlight.SetActive(false);
                messageManager.showMessage("Die Taschenlampe hat den Geist aufgegeben.");
            } else if (flashlightDuration <= flashlightFadeOut) {
                flashlightLight.intensity -= Time.deltaTime; // fade out 
            }
        } else {
            if (Input.GetKeyDown(playerData.taschenlampeKeyCode)) {
                activateFlashlight();
            }
        }
    }

    // JUST FOR TESTING!
    public void activateBirdIndicator() {
        birdCageIndicator.SetActive(true);
        birdCageIndicatorRawImage.enabled = true;
        birdCageIndicatorActive = true;
    } // REMOVE LATER ON

    public void updateBirdCageIndicator() {
        // sing etc. Animation
    }
    
    public void turnOnRadio() {
        fadeRadioVolumeIn();
        Invoke("fadeRadioVolumeOut", 5.0f);
    }

    private void fadeRadioVolumeIn() {
        iTween.AudioTo(radioAudioSourceGameObject, 1.0f, 1.0f, 5.0f);
    }

    private void fadeRadioVolumeOut() {
        iTween.AudioTo(radioAudioSourceGameObject, 0.0f, 1.0f, 5.0f);
    }

    public void toggleShowDiary() {
        diaryRawImage.enabled = !diaryRawImage.enabled;
    }

    public void showDiary(bool show) {
        diaryRawImage.enabled = show;
    }

    public bool isDiaryVisible() {
        return diaryRawImage.enabled;
    }

    public void setInjured(bool injured) {
        isInjured = injured;
        if (injured) {
            firstPersonController.SetMovementSpeed(
                playerData.injuredWalkSpeed,
                playerData.injuredRunSpeed,
                playerData.injuredJumpSpeed
                );
        } else {
            firstPersonController.SetMovementSpeed(
                playerData.walkSpeed,
                playerData.runSpeed,
                playerData.jumpSpeed
                );
        }
    }
    #endregion



    #region helper
    private void tryHitObject() {
        Ray ray = new Ray(fpsCamera.transform.position + playerData.raycastOffset, fpsCamera.transform.TransformDirection(Vector3.forward));
        int interactableLayer = 8;
        int interactableLayerMask = 1 << interactableLayer;
        
        if (Physics.Raycast(ray, out raycastHit, playerData.raycastMaxDistance)) { // test if hitting anything
            if ((raycastHit.collider.gameObject.layer & interactableLayer) != 0) { // test if hitting interactable
                setQuestItemName(raycastHit.collider.name);
            } else {
                setQuestItemName(null);
            }
        } else {
            setQuestItemName(null);
        }
    }

    private void setQuestItemName(string text) {
        if (!questItemNameText.text.Equals(text)) {
            questItemNameText.text = text;
        }
    }

    public void loadGameScene() {
        SceneManager.LoadScene("GameScene");
    }

    public void loadGameEndScene() {
        SceneManager.LoadScene("GameEndScene");
    }

    public void InitZahlenSchloss() {
        System.Random random = new System.Random();
        int wheelStepCount = openLockQuestAction.turnLockWheelQuestActions[0].wheelStepCount;
        zalenCodeText.text = string.Empty;
        for (int i= 0; i < openLockQuestAction.lockCode.Length; i++) {
            openLockQuestAction.lockCode[i] = random.Next(0, wheelStepCount);
            //Debug.Log(i + " " + openLockQuestAction.lockCode[i]);
            zalenCodeText.text += openLockQuestAction.lockCode[i].ToString();
        }
    }

    public void Die() {
        if (!isDead) {
            isDead = true;
            fadingLayerScript.fadeLayerIn();
            Invoke("loadGameScene", fadingLayerScript.fadingDelay + fadingLayerScript.fadingDuration);
        }
    }
    #endregion
}