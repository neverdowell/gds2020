﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "NewGame/Quest Data")]
public class QuestData : ScriptableObject
{
    public string name;
    public List<Action> successfullActions;
    public string successMessage = "Das hat geklappt.";
    public string failMessage = "Das funktioniert so nicht.";
    public ItemData[] requiredItems;
    public ItemData[] AddToInventory;
    public ItemData[] RemoveFromInventory;
    public bool destroyInteractable;
    public float timeToDestroyInteractable;
    public GameObject spawnGameObject = null;
    public Vector3 spawnPosition = Vector3.zero;
    public Vector3 spawnRotation = Vector3.zero;
}
