﻿using UnityEngine;

[CreateAssetMenu(menuName = "DatingSim/Create new ItemData")]
public class ItemData : ScriptableObject {
    [Header("Item Data")]
    public int itemId;
    public string name;
    public GameObject prefab;
    public Sprite sprite;
}
