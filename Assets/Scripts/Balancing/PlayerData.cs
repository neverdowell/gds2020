﻿using UnityEngine;

[CreateAssetMenu(menuName = "NewGame/Player Data")]
public class PlayerData : ScriptableObject {

    [Header("Game Settings")]
    public float gameDuration = 5 * 60f;

    [Header("Input")]
    public KeyCode primaryAction = KeyCode.Mouse0;
    public KeyCode secondaryAction = KeyCode.Mouse1;
    public KeyCode tertiaryAction = KeyCode.Mouse2;
    public KeyCode tagebuchKeyCode = KeyCode.Q;
    public KeyCode taschenlampeKeyCode = KeyCode.E;
    public KeyCode inventoryKeyCode = KeyCode.I;
    public KeyCode cancelKeyCode = KeyCode.Escape;
    public float mouseSensitivityX = 2;
    public float mouseSensitivityY = 2;

    [Header("Physics")]
    public float walkSpeed = 5f;
    public float runSpeed = 10f;
    public float jumpSpeed = 10f;
    public float injuredWalkSpeed = 2f;
    public float injuredRunSpeed = 4f;
    public float injuredJumpSpeed = 4f;
    
    public Vector3 raycastOffset = Vector3.zero; // unnötig?
    public float raycastMaxDistance = 5;

    [Header("Inventory")]
    public int sizeOfInventory = 10;
}
