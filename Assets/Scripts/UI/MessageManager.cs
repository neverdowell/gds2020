﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageManager : MonoBehaviour{

    [Header("Message UI")]
    public GameObject messageUIGameObject;
    public Text messageText;

    [Header("QuestItem name UI")]
    public GameObject questItemUIGameObject;
    public Text questItemText;

    [Header("Settings")]
    public float fadingDelay = 2;
    public float fadingDuration = 0.5f;



    private Queue<string> messages = new Queue<string>();
    private bool isMessageShown = false;
    private Hashtable messageColorFadingHashTable;




    #region singleton
    private static MessageManager instance;
    private MessageManager() { }

    public static MessageManager GetInstance() {
        return instance;
    }
    #endregion




    #region lifecycle
    private void Awake() {
        if (instance == null) {
            instance = this;
            init();
        } else {
            if (instance != this) {
                Destroy(this);
            }
        }
    }
    #endregion




    #region helper
    void init() {
        messageColorFadingHashTable = iTween.Hash(
            "from", 1f,
            "to", 0f,
            "delay", fadingDelay,
            "time", fadingDuration,
            "onupdate", "updateMessageColor",
            "oncomplete", "onMessageShowComplete"
        );
    }

    public void showMessage(string messageText) {
        // is message is not already shown or in message queue -> show
        if (messageText != null && messageText.Length > 0 && !messages.Contains(messageText) && this.messageText.text != messageText) {
            // if a message is shown enqueue otherwise show
            if (isMessageShown) { 
                messages.Enqueue(messageText);
            } else { 
                this.messageText.text = messageText;
                this.messageText.color = Color.white;
                isMessageShown = true;
                iTween.ValueTo(this.gameObject, messageColorFadingHashTable);
            }
        } else {
            Debug.Log("MessageManager.showMessage: Ignore duplicate message");
        }
    }

    public void updateMessageColor (float val) {
        messageText.color = new Color(1,1,1,val);
    }

    public void onMessageShowComplete() {
        isMessageShown = false;
        messageText.text = null;
        // if more messages are in queue -> show
        if (messages.Count > 0) {
            showMessage(messages.Dequeue());
        }
    }
    #endregion
}
