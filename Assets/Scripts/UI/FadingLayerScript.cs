﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FadingLayerScript : MonoBehaviour{

    public Color layerColor;
    public float fadingDuration;
    public float fadingDelay;
    public float minAlpha = 0f;
    public float maxAlpha = 1f;
    public Image image;


    private Hashtable colorFadingOutHashTable;
    private Hashtable colorFadingInHashTable;


    void Awake(){
        colorFadingOutHashTable = iTween.Hash(
            "from", maxAlpha,
            "to", minAlpha,
            "delay", fadingDelay,
            "time", fadingDuration,
            "onupdate", "updateLayerColor"
        );

        colorFadingInHashTable = iTween.Hash(
            "from", minAlpha,
            "to", maxAlpha,
            "delay", fadingDelay,
            "time", fadingDuration,
            "onupdate", "updateLayerColor"
            );
        image.enabled = true;
    }


    public void fadeLayerIn() {
        iTween.ValueTo(this.gameObject, colorFadingInHashTable);
    }

    public void fadeLayerOut() {
        iTween.ValueTo(this.gameObject, colorFadingOutHashTable);
    }

    public void updateLayerColor(float alphaValue ) {
        image.color = new Color(layerColor.r, layerColor.g, layerColor.b, alphaValue);
    }
}
