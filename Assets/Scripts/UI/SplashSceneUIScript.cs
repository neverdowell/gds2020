﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashSceneUIScript : MonoBehaviour{

    public FadingLayerScript fadingLayerScript;





    #region lifecycle
    private void Start() {
        fadingLayerScript.fadeLayerOut();
    }

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            fadingLayerScript.fadeLayerIn();
            Invoke("LoadMenuScene", 2f);
        }
    }
    #endregion




    #region helper
    public void LoadMenuScene() {
        SceneManager.LoadScene("MenuScene");
    }
    #endregion
}
