﻿using UnityEngine;

public class InventoryUIScript : MonoBehaviour{

    public GameObject contentGameObject;
    public GameObject inventoryItemPrefab;
    public GameObject emptyInventoryItemPrefab;



    private InventoryManager inventoryManager;




    #region lifecycle
    private void Start(){
        inventoryManager = InventoryManager.GetInstance();
        inventoryManager.inventoryUIScript = this;
        RefreshInventory();
        gameObject.SetActive(false);
    }

    private void OnDestroy() {
        inventoryManager.inventoryUIScript = null;
    }
    #endregion




    #region helper
    public void RefreshInventory() {
        // clear inventory items
        foreach(Transform child in contentGameObject.transform) {
            Destroy(child.gameObject);
        }

        if (inventoryManager.inventory.Count == 0) {
            Instantiate(emptyInventoryItemPrefab, contentGameObject.transform);
        } else { 
            // add current items
            foreach (ItemData itemData in inventoryManager.inventory) {
                GameObject inventoryItem = Instantiate(inventoryItemPrefab, contentGameObject.transform);
                inventoryItem.name = itemData.name;
                InventoryItemUIScript inventoryItemUIScript = inventoryItem.GetComponent<InventoryItemUIScript>();
                inventoryItemUIScript.itemData = itemData;
                inventoryItemUIScript.inventoryItemImage.sprite = itemData.sprite;
                inventoryItemUIScript.inventoryItemName.text = itemData.name;
            }
        }
    }
    #endregion
}
