﻿using UnityEngine;

public class MenuUIScript : MonoBehaviour{

    public FadingLayerScript fadingLayerScript;
    public GameObject ingameMenu;



    private GameManager gameManager;
    private PlayerScript playerScript;



    #region lifecycle
    private void Start() {
        gameManager = GameManager.GetInstance();
        playerScript = GameObject.Find("Player").GetComponent<PlayerScript>();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (ingameMenu.activeSelf) {
                ingameMenu.SetActive(false);
                gameManager.ShowCursor(false);
                gameManager.SetActiveMouseLookAndHover(true);

            } else {
                ingameMenu.SetActive(true);
                gameManager.ShowCursor(true);
                gameManager.SetActiveMouseLookAndHover(false);
            }
        }
    }
    #endregion
}
