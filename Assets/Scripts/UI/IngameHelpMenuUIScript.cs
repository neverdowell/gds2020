﻿using UnityEngine;

public class IngameHelpMenuUIScript : MonoBehaviour{

    public GameObject ingameMenu;
    public GameObject ingameHelpMenu;
    public bool closeOnAnyKeyOrButton = false;


    #region
    public void Start() {
        // setze bei gelegenheit mal die korrekten Werte aus der Konfigdatei
    }

    public void Update()
    {
        if (closeOnAnyKeyOrButton && Input.anyKeyDown)
        {
            Back();
        }
    }

    #endregion


    #region ui callbacks
    public void Back() {
        if(ingameMenu != null) { 
            ingameMenu.SetActive(true);
        }
        ingameHelpMenu.SetActive(false);
    }
    #endregion
}
