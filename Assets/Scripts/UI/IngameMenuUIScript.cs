﻿using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class IngameMenuUIScript : MonoBehaviour{

    public GameObject ingameMenu;
    public GameObject ingameHelpMenu;


    private GameManager gameManager;
    private PlayerScript playerScript;


    #region
    public void Start() {
        gameManager = GameManager.GetInstance();
        playerScript = GameObject.Find("Player").GetComponent<PlayerScript>();
    }
    #endregion


    #region ui callbacks
    public void Resume() {
        GameManager.GetInstance().ShowCursor(false);
        ingameMenu.SetActive(false);
        gameManager.SetActiveMouseLookAndHover(true);
    }

    public void Restart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ShowIngameHelp() { 
        ingameMenu.SetActive(false);
        ingameHelpMenu.SetActive(true);
    }


    public void ExitGame() {
        Debug.Log("Quit");
        GameManager.GetInstance().ShowCursor(false);
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else 
        Application.Quit();
#endif
    }
    #endregion
}
