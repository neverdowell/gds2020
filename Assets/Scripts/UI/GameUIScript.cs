﻿using UnityEngine;
using UnityEngine.UI;

public class GameUIScript : MonoBehaviour{

    public GameObject ingameMenu;
    public GameObject ingameHelpMenu;
    public Text timerText;
    public float currentTimer;

    private GameManager gameManager;
    private PlayerScript playerScript;
    



    #region lifecycle
    private void Start() {
        gameManager = GameManager.GetInstance();
        playerScript = GameObject.Find("Player").GetComponent<PlayerScript>();
        currentTimer = playerScript.playerData.gameDuration;
        MessageManager.GetInstance().showMessage("Drücke ESC für Hilfe");
    }

    void Update() {
        UpdateTimer();

        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (ingameHelpMenu.activeSelf) { 
                ingameHelpMenu.SetActive(false);
                ingameMenu.SetActive(true);
            } else if (ingameMenu.activeSelf) {
                ingameMenu.SetActive(false);
                gameManager.ShowCursor(false);
                gameManager.SetActiveMouseLookAndHover(true);
            } else {
                ingameMenu.SetActive(true);
                gameManager.ShowCursor(true);
                gameManager.SetActiveMouseLookAndHover(false);
            }
        }
    }


    public void UpdateTimer() {
        if (!gameManager.pauseTimer) { 
            currentTimer -= Time.deltaTime;
        
            if (currentTimer > 0) { 
                float minutes = Mathf.Floor(currentTimer / 60);
                int seconds = (int)currentTimer % 60;

                if (seconds < 10) {
                    timerText.text = minutes + ":0" + seconds;
                } else {
                    timerText.text = minutes + ":" + seconds;
                }
            } else {
                playerScript.Die();
            }
        }
    }
    #endregion
}
