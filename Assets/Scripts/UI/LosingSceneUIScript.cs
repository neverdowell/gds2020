﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LosingSceneUIScript : MonoBehaviour{

    public FadingLayerScript fadingLayerScript;

    private string sceneToLoad;


    #region lifecycle
    private void Start() {
        fadingLayerScript.fadeLayerOut();
    }

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            
        }
    }
    #endregion




    #region
    public void OnRestartButtonClicked() {
        fadingLayerScript.fadeLayerIn();
        sceneToLoad = "GameScene";
        Invoke("LoadScene", 2f);
    }

    public void OnMenuButtonClicked() {
        fadingLayerScript.fadeLayerIn();
        sceneToLoad = "MenuScene";
        Invoke("LoadScene", 2f);
    }

    public void OnExitButtonClicked() {
        fadingLayerScript.fadeLayerIn();
        sceneToLoad = "LoadMenuScene";
        Invoke("ExitGame", 2f);
    }
    #endregion




    #region helper
    public void LoadScene() {
        SceneManager.LoadScene(sceneToLoad);
    }

    public void ExitGame() {
        if (Application.isEditor) {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        } else { 
            Application.Quit();
        }
    }


#endregion
}
