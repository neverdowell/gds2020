﻿using UnityEngine;

public class MineCartMovementScript : MonoBehaviour{

    public GameObject[] targetPositions;
    public float speed;
    public float maxRadiansDelta;
    public float maxMagnitudeDelta;
    public bool isMoving = false;
    public bool isWheelRepaired = false;
    public bool isRailStopperBlockRemoved = false;
    public bool hasBeenMoved = false;
    public GameObject wallToDestroy;
    



    private int currentTargetPositionIndex = 0;




    #region lifecycle
    void Update(){
        // handle movement
        if (isMoving) {
            // speed up
            speed += Time.deltaTime * 2;
            // move and rotate
            transform.position = Vector3.MoveTowards(transform.position, targetPositions[currentTargetPositionIndex].transform.position, speed * Time.deltaTime);
            transform.eulerAngles = Vector3.RotateTowards(transform.localRotation.eulerAngles, targetPositions[currentTargetPositionIndex].transform.localRotation.eulerAngles, maxRadiansDelta, maxMagnitudeDelta);
            // check if next targetPosition
            if (transform.position.Equals(targetPositions[currentTargetPositionIndex].transform.position)){
                currentTargetPositionIndex++;
                if (isRailStopperBlockRemoved) { 
                    if (currentTargetPositionIndex == targetPositions.Length) {
                        isMoving = false;
                        hasBeenMoved = true;
                        Destroy(wallToDestroy);
                    }
                } else {
                    if (currentTargetPositionIndex == targetPositions.Length - 1) {
                        hasBeenMoved = true;
                        isMoving = false;
                    }
                }
            }
        }
    }
    #endregion




    #region helper
    public void RepairBrokenWheel() {
        isWheelRepaired = true;
    }

    public void PushMineCart() {
        if (isWheelRepaired && !hasBeenMoved) { 
            isMoving = true;
        }
    }

    #endregion
}
