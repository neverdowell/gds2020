﻿using UnityEngine;

public class BirdAnimationScript : MonoBehaviour{

    public GameObject head;
    public GameObject lowerBeack;
    public GameObject leftEye;
    public GameObject rightEye;
    public GameObject leftWing;
    public GameObject rightWing;
    public GameObject tailFeather;

    public float shakeHeadDuration;
    public float twinkerDuration;
    public float singDuration;



    public bool isDead = false;
    public bool isParalyzed = false;
    public float minDurationUntilNextAnimation;
    public float currentDurationUntilNextAnimation;



    void Start(){
        
    }

    void Update(){
        
    }

    #region bird animation
    public void shakeHead() {
        if (!isDead || !isParalyzed) {

        }
    }

    public void twinker() {
        if (!isDead || !isParalyzed) {

        }
    }

    public void sing() {
        if (!isDead || !isParalyzed) {

        }
    }

    public void flapWings() {
        if (!isDead || !isParalyzed) {

        }
    }

    public void flapTailFeather() {
        if (!isDead || !isParalyzed) {

        }
    }

    public void jump() {
        if (!isDead || !isParalyzed) {

        }
    }

    #endregion


    #region helper
    public void die() {
        isDead = true;
    }

    public void paralyze() {
        isParalyzed = true;
    }

    public void startRandomAnimation() {
        int animationIndex = (int)Random.Range(0, 5);

        switch (animationIndex) {
            case 0:
                shakeHead();
                break;
            case 1:
                twinker();
                break;
            case 2:
                sing();
                break;
            case 3:
                flapWings();
                break;
            case 4:
                flapTailFeather();
                break;
            case 5:
                jump();
                break;
            default:
                Debug.Log("index out of bounds");
                break;
        }
    }
    #endregion
}
