﻿using System.Collections;
using UnityEngine;

public class DoorMovementScript : MonoBehaviour{
    public bool openAtStart = true;
    public Vector3 closedAngles = new Vector3(0, 0, 0);
    public Vector3 openAngles = new Vector3(0, 90, 0);
    public float movementDuration = 1.0f;


    private bool isOpen = false;
    private bool isMoving = false;
    private Hashtable openHashtable;
    private Hashtable closeHashtable;



    #region lifecycle
    void Start(){
        if (openAtStart) {
            transform.localRotation = Quaternion.Euler(openAngles);
            isOpen = true;
        } else {
            transform.localRotation = Quaternion.Euler(closedAngles);
            isOpen = false;
        }

        openHashtable = iTween.Hash(
            "rotation", openAngles,
            "oncomplete", "OnMovementComplete",
            "duration", movementDuration
            );
        closeHashtable = iTween.Hash(
            "rotation", closedAngles,
            "oncomplete", "OnMovementComplete",
            "duration", movementDuration
            );
    }

    // for testing only
    private void Update() {
        if (Input.GetKeyDown(KeyCode.O)) {
            //if (transform.localRotation.eulerAngles.Equals(openAngles)) {
            if (isOpen) { 
                close();
            } else {
                open();
            }
        }
    }
    #endregion




    #region helper
    public void moveDoor() {
        if (isOpen) {
            close();
        } else {
            open();
        }
    }

    public void open() {
        if (!isMoving) { 
            isMoving = true;
            isOpen = true;
            iTween.RotateTo(gameObject, openHashtable);
        }
    }

    public void close() {
        if (!isMoving) {
            isMoving = true;
            isOpen = false;
            iTween.RotateTo(gameObject, closeHashtable);
        }
    }

    public void OnMovementComplete() {
        isMoving = false;
    }
    #endregion helper
}
